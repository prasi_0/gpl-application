﻿using System.Collections.Generic;
using System.Windows.Forms;

namespace GPL___Application
{
    /// <summary>
    /// Dictionary class to assign functions and values into key-value pairs
    /// </summary>
    class SaveFunction
    {
        private static Dictionary<string, int> _savefunction = new Dictionary<string, int>();
        public static Dictionary<string, int> Savefunction
        {
            get
            {
                return _savefunction;
            }
            set
            {
                _savefunction = value;
            }
        }
        /// <summary>
        /// Method to clear functions
        /// </summary>
        public static void Clear()
        {
            _savefunction = new Dictionary<string, int>();
        }
        /// <summary>
        /// Searches through functions defined
        /// </summary>
        /// <param name="var"></param>
        /// <returns></returns>
        public static int SearchFunction(string var)
        {
            int finalOutput;

            if (!_savefunction.TryGetValue(var, out finalOutput))
            {
                MessageBox.Show("No function defined", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return finalOutput;
        }
    }
}
