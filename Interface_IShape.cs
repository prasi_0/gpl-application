﻿using System.Drawing;

namespace GPL___Application
{
    /// <summary>
    /// Interface with method declarations called IShape 
    /// referenced to later implement in all classes
    /// </summary>
    interface Interface_IShape
    {
        void set(Color c, bool fill, params int[] list);
        void set(string flash, bool fill, params int[] list);
        void draw(Graphics g);
    }
}
