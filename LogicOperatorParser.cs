﻿using System;
using System.Windows.Forms;

namespace GPL___Application
{
    /// <summary>
    /// Class to split commands with given logic operators 
    /// and function to carry when each logic operator is given
    /// </summary>
    class LogicOperatorParser
    {
        OperatorParser operatorParser = new OperatorParser();
        public bool Parse(string command)
        {
            //check if a command is given and split at logic operators
            string[] input = command.Split(new string[] { "==", "!=", "<=", ">=", "<", ">" }, StringSplitOptions.RemoveEmptyEntries);
            if (input.Length == 0)
            {
                MessageBox.Show("No command found", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (input.Length == 1)
            {
                return false;
            }
            else
            {
                //check if logic operator is given and perform operations
                string giveOperator = command.Replace(input[0], "").Replace(input[1], "").Trim();
                if (giveOperator.Length == 0)
                {
                    MessageBox.Show("No logic operator found", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                int value1 = operatorParser.Parse(input[0].Trim());
                int value2 = operatorParser.Parse(input[1].Trim());

                switch (giveOperator)
                {
                    case "==":
                        return value1 == value2;
                    case "<":
                        return value1 < value2;
                    case ">":
                        return value1 > value2;
                    case "!=":
                        return value1 != value2; 
                    case "<=":
                        return value1 <= value2;
                    case ">=":
                        return value1 >= value2;
                }
            }
            return false;
        }
    }
}
