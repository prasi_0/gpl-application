﻿using System.Drawing;

namespace GPL___Application
{
    /// <summary>
    /// DrawTo class extending Shape as base class
    /// </summary>
    class DrawTo : Shape
    {
        int x2, y2;
        public DrawTo() : base()
        {

        }

        public DrawTo(Color color, bool fill, int x, int y, int x2, int y2) : base(color, fill, x, y)
        {
            this.x2 = x2;
            this.y2 = y2;
        }

        public override void draw(Graphics g)
        {
            Pen p = new Pen(color, 3);
            g.DrawLine(p, x, y, x2, y2);
        }

        public override void set(Color color, bool fill, params int[] list)
        {
            base.set(color, fill, list[0], list[1]);
            this.x2 = list[2];
            this.y2 = list[3];
        }

    }
}
