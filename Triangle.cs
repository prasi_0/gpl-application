﻿using System.Drawing;
using System.Windows.Forms;
using System.Threading;

namespace GPL___Application
{
    /// <summary>
    /// Added new class Triangle extending the Shape class
    /// </summary>
    public class Triangle : Shape
    {
        int x2, y2, x3, y3;
        /// <summary>
        /// Reuse and access base class by extending it
        /// </summary>
        public Triangle() : base()
        {

        }
        /// <summary>
        /// All parameters needed to create the Triangle shape are clearly defined
        /// </summary>
        /// <param name="color"></param>
        /// <param name="fill"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="x2"></param>
        /// <param name="y2"></param>
        /// <param name="x3"></param>
        /// <param name="y3"></param>
        public Triangle(Color color, bool fill, int x, int y, int x2, int y2, int x3, int y3) : base(color, fill, x, y)
        {
            this.x2 = x2;
            this.y2 = y2;
            this.x3 = x3;
            this.y3 = y3;
        }
        /// <summary>
        /// New parameter flash added to list of params in base class
        /// </summary>
        /// <param name="flash"></param>
        /// <param name="fill"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="x2"></param>
        /// <param name="y2"></param>
        /// <param name="x3"></param>
        /// <param name="y3"></param>
        public Triangle(string flash, bool fill, int x, int y, int x2, int y2, int x3, int y3) : base(flash, fill, x, y)
        {
            this.x2 = x2;
            this.y2 = y2;
            this.x3 = x3;
            this.y3 = y3;
        }

        /// <summary>
        /// All params are instantiated and stored into int[] list
        /// </summary>
        /// <param name="color"></param>
        /// <param name="fill"></param>
        /// <param name="list"></param>
        public override void set(Color color, bool fill, params int[] list)
        {
            base.set(color, fill, list[0], list[1]);
            this.x2 = list[2];
            this.y2 = list[3];
            this.x3 = list[4];
            this.y3 = list[5];
        }
        /// <summary>
        /// New implementation of set with flash from base class
        /// </summary>
        /// <param name="flash"></param>
        /// <param name="fill"></param>
        /// <param name="list"></param>
        public override void set(string flash, bool fill, params int[] list)
        {
            base.set(flash, fill, list[0], list[1]);
            this.x2 = list[2];
            this.y2 = list[3];
            this.x3 = list[4];
            this.y3 = list[5];
        }
        /// <summary>
        /// Draw method which draws the shape with
        /// condition between fill or flash 
        /// </summary>
        /// <param name="g"></param>
        public override void draw(Graphics g)
        {
            Point[] points = new Point[3];
            points[0].X = this.x;
            points[0].Y = this.y;
            points[1].X = this.x2;
            points[1].Y = this.y2;
            points[2].X = this.x3;
            points[2].Y = this.y3;

            if (fill)
            {
                if (flash != null)
                {
                    Thread newThread;
                    switch (flash)
                    {
                        case "redgreen":
                            newThread = new Thread(() => GetFlash(g, points, Color.Red, Color.Green));
                            newThread.IsBackground = true;
                            newThread.Start();
                            break;
                        case "blueyellow":
                            newThread = new Thread(() => GetFlash(g, points, Color.Blue, Color.Yellow));
                            newThread.IsBackground = true;
                            newThread.Start();
                            break;
                        case "blackwhite":
                            newThread = new Thread(() => GetFlash(g, points, Color.Black, Color.White));
                            newThread.IsBackground = true;
                            newThread.Start();
                            break;
                        default:
                            MessageBox.Show("Invalid color value", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            break;
                    }
                }
                else
                {
                    SolidBrush b = new SolidBrush(color);
                    g.FillPolygon(b, points);
                }
            }
            else
            {
                Pen p = new Pen(color, 2);
                g.DrawPolygon(p, points);
            }
        }

        /// <summary>
        /// Methos to get the shape flashing
        /// </summary>
        /// <param name="g"></param>
        /// <param name="points"></param>
        /// <param name="first"></param>
        /// <param name="second"></param>
        private void GetFlash(Graphics g, Point[] points, Color first, Color second)
        {
            bool flag = false;
            while (true)
            {
                lock (g)
                {
                    if (flag == false)
                    {
                        SolidBrush b = new SolidBrush(first);
                        g.FillPolygon(b, points);
                        flag = true;
                    }
                    else
                    {
                        SolidBrush b = new SolidBrush(second);
                        g.FillPolygon(b, points);
                        flag = false;
                    }
                }
                Thread.Sleep(500);
            }
        }
        /// <summary>
        /// Overrides ToString method to convert input to string
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return base.ToString() + " " + this.x2 + " " + this.y2 + " " + this.x3 + " " + this.y3;
        }
    }
}
