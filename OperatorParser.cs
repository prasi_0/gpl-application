﻿using System;
using System.Windows.Forms;

namespace GPL___Application
{
    /// <summary>
    /// Class to split commands with given operators 
    /// and function to carry when each operator is given
    /// </summary>
    class OperatorParser
    {
        public int Parse(string expression)
        {
            //check if command is given and split at operator
            string[] operands = expression.Split(new string[] { "+", "*", "/", "-","%" }, StringSplitOptions.RemoveEmptyEntries);
            if (operands.Length == 0)
            {
                MessageBox.Show("No operands found", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (operands.Length == 1)
            {
                return Assign.findVariable(operands[0]);
            }
            else
            {
                //check if operator is given and which operation to perform
                string giveOperator = expression.Replace(operands[0], "").Replace(operands[1], "").Trim();
                if (giveOperator.Length == 0)
                {
                    MessageBox.Show("No operator found", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                int value1 = Assign.findVariable(operands[0].Trim());
                int value2 = Assign.findVariable(operands[1].Trim());

                switch (giveOperator)
                {
                    case "+":
                        return value1 + value2;
                    case "*":
                        return value1 * value2;
                    case "/":
                        return value1 / value2;
                    case "-":
                        return value1 - value2;
                    case "%":
                        return value1 % value2;
                }
            }
            return 0;
        }

    }
}
