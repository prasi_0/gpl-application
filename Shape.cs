﻿using System.Drawing;


namespace GPL___Application
{

    /// <summary>
    /// Create base class extending interface Interface_IShape 
    /// </summary>
    public abstract class Shape : Interface_IShape
    {
        /// <summary>
        /// Represents alpha, red , green , blue colors with Color 
        /// </summary>
        protected Color color;
        /// <summary>
        /// Boolean fill to check if fill is given
        /// </summary>
        protected bool fill;
        /// <summary>
        /// Common points x and y for all shapes
        /// </summary>
        protected int x, y;
        /// <summary>
        /// Flash string to check the null value
        /// </summary>
        protected string flash;

        /// <summary>
        /// Set default pen color to black 
        /// Set default x and y position to (50,50)
        /// </summary>
        public Shape()
        {
            color = Color.Black;
            x = y = 50;
        }
        /// <summary>
        /// Base method for shapes to extend to use color
        /// </summary>
        /// <param name="color"></param>
        /// <param name="fill"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public Shape(Color color, bool fill, int x, int y)
        {
            this.color = color;
            this.x = x;
            this.y = y;
            this.fill = fill;
        }
        /// <summary>
        /// Base method for shapes to extend to use flash
        /// </summary>
        /// <param name="flash"></param>
        /// <param name="fill"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public Shape(string flash, bool fill, int x, int y)
        {
            this.flash = flash;
            this.fill = fill;
            this.x = x;
            this.y = y;
        }
        /// <summary>
        /// Draw method to draw within draw surface
        /// </summary>
        /// <param name="g"></param>
        public abstract void draw(Graphics g);

        /// <summary>
        /// Virtual methos can be overridden 
        /// Set values of all the parameters 
        /// </summary>
        /// <param name="color"></param>
        /// <param name="fill"></param>
        /// <param name="list"></param>
        public virtual void set(Color color, bool fill, params int[] list)
        {
            this.color = color;
            this.fill = fill;
            this.x = list[0];
            this.y = list[1];
        }
        /// <summary>
        /// Set method to set values of all params
        /// </summary>
        /// <param name="flash"></param>
        /// <param name="fill"></param>
        /// <param name="list"></param>
        public virtual void set(string flash, bool fill, params int[] list)
        {
            this.flash = flash;
            this.fill = fill;
            this.x = list[0];
            this.y = list[1];
        }

        /// <summary>
        /// Method to change input given as string
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return base.ToString() + " " + this.x + "," + this.y + " : ";
        }

    }
}
