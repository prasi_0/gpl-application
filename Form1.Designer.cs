﻿
namespace GPL___Application
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutUsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pictureBox_output = new System.Windows.Forms.PictureBox();
            this.textBox_singleLine_cmd = new System.Windows.Forms.TextBox();
            this.button_Run = new System.Windows.Forms.Button();
            this.button_Reset = new System.Windows.Forms.Button();
            this.textBox_multiLine_cmd = new System.Windows.Forms.TextBox();
            this.button_Save = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_output)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.aboutToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(800, 28);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            this.toolTip1.SetToolTip(this.menuStrip1, "Open menu for more actions");
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newFileToolStripMenuItem,
            this.openFileToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(46, 24);
            this.fileToolStripMenuItem.Text = "File";
            this.fileToolStripMenuItem.ToolTipText = "File menu - Open for actions";
            // 
            // newFileToolStripMenuItem
            // 
            this.newFileToolStripMenuItem.Name = "newFileToolStripMenuItem";
            this.newFileToolStripMenuItem.Size = new System.Drawing.Size(128, 26);
            this.newFileToolStripMenuItem.Text = "New ";
            this.newFileToolStripMenuItem.Click += new System.EventHandler(this.newFileToolStripMenuItem_Click);
            // 
            // openFileToolStripMenuItem
            // 
            this.openFileToolStripMenuItem.Name = "openFileToolStripMenuItem";
            this.openFileToolStripMenuItem.Size = new System.Drawing.Size(128, 26);
            this.openFileToolStripMenuItem.Text = "Open";
            this.openFileToolStripMenuItem.Click += new System.EventHandler(this.openFileToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(128, 26);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(128, 26);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutUsToolStripMenuItem});
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(64, 24);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.ToolTipText = "About menu";
            // 
            // aboutUsToolStripMenuItem
            // 
            this.aboutUsToolStripMenuItem.Name = "aboutUsToolStripMenuItem";
            this.aboutUsToolStripMenuItem.Size = new System.Drawing.Size(153, 26);
            this.aboutUsToolStripMenuItem.Text = "About Us";
            this.aboutUsToolStripMenuItem.Click += new System.EventHandler(this.aboutUsToolStripMenuItem_Click);
            // 
            // pictureBox_output
            // 
            this.pictureBox_output.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox_output.Location = new System.Drawing.Point(356, 36);
            this.pictureBox_output.Name = "pictureBox_output";
            this.pictureBox_output.Size = new System.Drawing.Size(434, 404);
            this.pictureBox_output.TabIndex = 1;
            this.pictureBox_output.TabStop = false;
            this.toolTip1.SetToolTip(this.pictureBox_output, "Canvas for displaying the result\r\n\r\n\r\n\r\n");
            // 
            // textBox_singleLine_cmd
            // 
            this.textBox_singleLine_cmd.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.textBox_singleLine_cmd.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.textBox_singleLine_cmd.Location = new System.Drawing.Point(12, 308);
            this.textBox_singleLine_cmd.Multiline = true;
            this.textBox_singleLine_cmd.Name = "textBox_singleLine_cmd";
            this.textBox_singleLine_cmd.Size = new System.Drawing.Size(338, 76);
            this.textBox_singleLine_cmd.TabIndex = 2;
            this.toolTip1.SetToolTip(this.textBox_singleLine_cmd, "Add single line command");
            this.textBox_singleLine_cmd.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox_singleLine_cmd_KeyDown);
            // 
            // button_Run
            // 
            this.button_Run.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.button_Run.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.button_Run.Location = new System.Drawing.Point(12, 390);
            this.button_Run.Name = "button_Run";
            this.button_Run.Size = new System.Drawing.Size(101, 50);
            this.button_Run.TabIndex = 3;
            this.button_Run.Text = "Run";
            this.toolTip1.SetToolTip(this.button_Run, "Run given commands");
            this.button_Run.UseVisualStyleBackColor = false;
            this.button_Run.Click += new System.EventHandler(this.button_Run_Click);
            // 
            // button_Reset
            // 
            this.button_Reset.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.button_Reset.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.button_Reset.Location = new System.Drawing.Point(119, 390);
            this.button_Reset.Name = "button_Reset";
            this.button_Reset.Size = new System.Drawing.Size(121, 50);
            this.button_Reset.TabIndex = 4;
            this.button_Reset.Text = "Reset";
            this.toolTip1.SetToolTip(this.button_Reset, "Reset all values");
            this.button_Reset.UseVisualStyleBackColor = false;
            this.button_Reset.Click += new System.EventHandler(this.button_Reset_Click);
            // 
            // textBox_multiLine_cmd
            // 
            this.textBox_multiLine_cmd.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.textBox_multiLine_cmd.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.textBox_multiLine_cmd.Location = new System.Drawing.Point(12, 36);
            this.textBox_multiLine_cmd.Multiline = true;
            this.textBox_multiLine_cmd.Name = "textBox_multiLine_cmd";
            this.textBox_multiLine_cmd.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox_multiLine_cmd.Size = new System.Drawing.Size(338, 266);
            this.textBox_multiLine_cmd.TabIndex = 5;
            this.toolTip1.SetToolTip(this.textBox_multiLine_cmd, "Add multiple line commands");
            // 
            // button_Save
            // 
            this.button_Save.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.button_Save.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.button_Save.Location = new System.Drawing.Point(246, 390);
            this.button_Save.Name = "button_Save";
            this.button_Save.Size = new System.Drawing.Size(104, 50);
            this.button_Save.TabIndex = 6;
            this.button_Save.Text = "Save";
            this.toolTip1.SetToolTip(this.button_Save, "Save the commands");
            this.button_Save.UseVisualStyleBackColor = false;
            this.button_Save.Click += new System.EventHandler(this.button_Save_Click);
            // 
            // toolTip1
            // 
            this.toolTip1.Popup += new System.Windows.Forms.PopupEventHandler(this.toolTip1_Popup);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.button_Save);
            this.Controls.Add(this.textBox_multiLine_cmd);
            this.Controls.Add(this.button_Reset);
            this.Controls.Add(this.button_Run);
            this.Controls.Add(this.textBox_singleLine_cmd);
            this.Controls.Add(this.pictureBox_output);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Graphical Programming Language - Application";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_output)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutUsToolStripMenuItem;
        private System.Windows.Forms.PictureBox pictureBox_output;
        private System.Windows.Forms.TextBox textBox_singleLine_cmd;
        private System.Windows.Forms.Button button_Run;
        private System.Windows.Forms.Button button_Reset;
        private System.Windows.Forms.TextBox textBox_multiLine_cmd;
        private System.Windows.Forms.Button button_Save;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}

