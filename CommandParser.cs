﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Linq;

namespace GPL___Application
{
    /// <summary>
    /// CommandParser class created to excute commands with keywords
    /// to make shapes: circle, rectangle and triagle
    /// Includes single line command like clear, reset and run
    /// </summary>
    public class CommandParser
    {
        private static int x = 0, y = 0;
        private static Color color = Color.Black;
        private static bool fill;
        private string flash = null;

        static OperatorParser operatorParser = new OperatorParser();
        /// <summary>
        /// Clear method sets default value of 
        /// color to black , fill to false and 
        /// both x and y coordinates to 0 each
        /// </summary>
        public void clear()
        {
            color = Color.Black;
            fill = false;
            x = 0;
            y = 0;
            fill = false;
            flash = null;
        }
        /// <summary>
        ///Method parse with command and graphics param
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="g"></param>
        public void parse(string cmd, Graphics g)
        {
            Factory_Shape factory = new Factory_Shape();
            try
            {
                char[] seperator = { ' ' };
                string[] args = cmd.Split(seperator, 2);
                string command = args[0];
                string parameters = "";
                if (args.Length > 1)
                {
                    parameters = args[1];
                }
                switch (command.ToLower()) //make commands case insensitive
                {
                    case "":
                        break;

                    case "circle": 
                        int radius = operatorParser.Parse(parameters.Trim());
                        Shape circle = factory.getShape("circle");      
                        if (flash != null) //conditions if either flash or fill is null or not
                        {
                            circle.set(flash, fill, x, y, radius);
                        }
                        else
                        {
                            circle.set(color, fill, x, y, radius);
                        }
                        circle.draw(g);
                        break;

                    case "rectangle":
                        int width = operatorParser.Parse(parameters.Split(',')[0].Trim());
                        int height = operatorParser.Parse(parameters.Split(',')[1].Trim());
                        Shape rectangle = factory.getShape("rectangle");
                        if (flash != null)
                        {
                            rectangle.set(flash, fill, x, y, width, height);
                        }
                        else
                        {
                            rectangle.set(color, fill, x, y, width, height);
                        }
                        rectangle.draw(g);
                        break;

                    case "square":
                        int length = operatorParser.Parse(parameters.Split(',')[0].Trim());
                        int sideLength = operatorParser.Parse(parameters.Split(',')[1].Trim());
                        Shape square = factory.getShape("square");
                        if (flash != null)
                        {
                            square.set(flash, fill, x, y, length, sideLength);
                        }
                        else
                        {
                            square.set(color, fill, x, y, length, sideLength);
                        }
                        square.draw(g);
                        break;

                    case "triangle":
                        int x2 = operatorParser.Parse(parameters.Split(',')[0].Trim());
                        int y2 = operatorParser.Parse(parameters.Split(',')[1].Trim());
                        int x3 = operatorParser.Parse(parameters.Split(',')[2].Trim());
                        int y3 = operatorParser.Parse(parameters.Split(',')[3].Trim());
                        Shape triangle = factory.getShape("triangle");
                        if (flash != null)
                        {
                            triangle.set(flash, fill, x, y, x2, y2, x3, y3);
                        }
                        else
                        {
                            triangle.set(color, fill, x, y, x2, y2, x3, y3);
                        }
                        triangle.draw(g);
                        break;

                    case "moveto": //command to move shape to given co-ordinate
                        x = operatorParser.Parse(parameters.Split(',')[0].Trim());
                        y = operatorParser.Parse(parameters.Split(',')[1].Trim());
                        break;

                    case "drawto": //command to draw the shape
                        int xaxis2 = operatorParser.Parse(parameters.Split(',')[0].Trim());
                        int yaxis2 = operatorParser.Parse(parameters.Split(',')[1].Trim());
                        Shape line = factory.getShape("drawto");
                        line.set(color, fill, x, y, xaxis2, yaxis2);
                        line.draw(g);
                        break;

                    case "color":
                        try
                        {
                            if (parameters.Split(',').Length == 1)
                            {
                                if (parameters == "redgreen" || parameters == "blueyellow" || parameters == "blackwhite")
                                {
                                    flash = parameters;
                                }
                                else
                                {
                                    color = Color.FromName(parameters.Trim());
                                    flash = null;
                                }
                            }
                            else if (parameters.Split(',').Length == 3)
                            {
                                int red = operatorParser.Parse(parameters.Split(',')[0].Trim());
                                int green = operatorParser.Parse(parameters.Split(',')[1].Trim());
                                int blue = operatorParser.Parse(parameters.Split(',')[2].Trim());
                                color = Color.FromArgb(255, red, green, blue);
                                flash = null;
                            }
                            else if (parameters.Split(',').Length == 4)
                            {
                                int alpha = operatorParser.Parse(parameters.Split(',')[0].Trim());
                                int red = operatorParser.Parse(parameters.Split(',')[1].Trim());
                                int green = operatorParser.Parse(parameters.Split(',')[2].Trim());
                                int blue = operatorParser.Parse(parameters.Split(',')[3].Trim());
                                color = Color.FromArgb(alpha, red, green, blue);
                                flash = null;
                            }
                            else
                            {
                                MessageBox.Show("Color value given is not valid!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }


                        }
                        catch (FormatException)
                        {
                            MessageBox.Show("Color value given is not valid!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        break;

                    case "var":
                        {
                            char[] separate = { ' ' };
                            string[] expression = parameters.Split(separate, 2);
                            if (Assign.Assignvariable.ContainsKey(expression[0].Trim()))
                            {
                                MessageBox.Show("Undefined Constant", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

                            }
                            else
                            {
                                try
                                {
                                    Assign.Assignvariable.Add(expression[0].Trim(), operatorParser.Parse(expression[1].Trim()));
                                }
                                catch (Exception)
                                {
                                    Assign.Assignvariable.Add(expression[0].Trim(), 0);
                                }
                            }
                        }
                        break;

                    default: //checks command 
                        if (command.Contains('='))
                        {
                            string[] splits = command.Split('=');
                            Assign.Assignvariable[splits[0].Trim()] = operatorParser.Parse(splits[1].Trim());
                        }
                        else if (parameters.Split(',')[0].Trim().Contains('='))
                        {
                            Assign.Assignvariable[command] = operatorParser.Parse(parameters.Split(',')[0].Trim().Substring(1).Trim());
                        }
                        else
                            MessageBox.Show("Incorrect command!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        break;

                    case "fill": //fill the color
                        {
                            fill = bool.Parse(parameters.Split(',')[0].Trim());
                            break;
                        }

                    case "clear": //clear the canvas
                        {
                            g.Clear(Color.Gray);
                            break;
                        }
                }
            }
            catch (FormatException)
            {
                MessageBox.Show("Argument type does not match!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
