﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GPL___Application
{
    /// <summary>
    /// Dictionary class to assign variables and values into key-value pairs
    /// </summary>
    class Assign
    {
        /// <summary>
        /// Stores global data which can be accessed from other methods 
        /// </summary>
        private static Dictionary<string, int> _assignVariable = new Dictionary<string, int>();
        public static Dictionary<string, int> Assignvariable
        {
            get { 
                return _assignVariable; 
            }

            set {
                _assignVariable = value; 
            }
        }
        /// <summary>
        /// Method to clear assigned variable
        /// </summary>
        public static void Clear()
        {
            _assignVariable = new Dictionary<string, int>();
        }
        /// <summary>
        /// Searches through variables defined
        /// </summary>
        /// <param name="var"></param>
        /// <returns></returns>
        public static int findVariable(string var)
        { 
            int finalOutput;

            if (!_assignVariable.TryGetValue(var, out finalOutput))
            {
                try
                {
                    finalOutput = int.Parse(var);
                }
                catch (FormatException)
                {
                    MessageBox.Show("No variable found", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

            return finalOutput;

        }
    }
}
