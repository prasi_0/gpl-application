﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace GPL___Application
{
    /// <summary>
    /// Class Form 1 extending Forms
    /// includes action for form components
    /// </summary>
    public partial class Form1 : Form
    {
        CommandParser commandParser = new CommandParser();
        private string filePath = null;
        private bool fileSaved = true;
        private bool runProgram = true;
        private int[] loopList = new int[10];
        private int loopIndex = -1;
        private bool methodRunning = false;
        private int tempProgramIndex = -1; 
        /// <summary>
        /// Initializes canvas component
        /// </summary>
        public Form1()
        {
            InitializeComponent();
            g = pictureBox_output.CreateGraphics();
        }
        Graphics g;
        LogicOperatorParser logicOperatorParser = new LogicOperatorParser();

        private void resetAll()
        {
            loopIndex = -1;
            runProgram = true;
            Array.Clear(loopList, 0, 10);
            Assign.Clear();
        }

        private void button_Reset_Click(object sender, EventArgs e)
        {
            g.Clear(Color.LightGray);
            textBox_singleLine_cmd.Text = "";
            textBox_multiLine_cmd.Text = "";
            commandParser.clear();
        }

        private void button_Save_Click(object sender, EventArgs e)
        {
            saveFile();
        }

        private void button_Run_Click(object sender, EventArgs e)
        {
            resetAll();
            for (int i = 0; i < textBox_multiLine_cmd.Lines.Length; i++)
            {
                try
                {
                    char[] seperator = new char[] { ' ' };
                    string[] args = textBox_multiLine_cmd.Lines[i].Trim().Split(seperator, 2);
                    string command = args[0].ToLower();
                    string parameters = "";
                    if (args.Length > 1)
                    {
                        parameters = args[1];
                    }
                    if (methodRunning)
                    {
                        if (command == "endmethod")
                        {
                            methodRunning = false;
                        }
                    }
                    else
                    {
                        //increase loop index number if while command is given
                        if (command == "while")
                        {
                            loopList[++loopIndex] = i;
                            continue;
                        }
                        else if (command == "endwhile")
                        {
                            //determine value of i and loop index based on operators given 
                            if (loopIndex > -1) 
                            {
                                string[] commandParse = textBox_multiLine_cmd.Lines[loopList[loopIndex]].Trim().Split(seperator, 2);
                                string parsedCommand = commandParse[0].ToLower();
                                string parsedParameters = "";

                                if (commandParse.Length > 1)
                                {
                                    parsedParameters = commandParse[1];
                                }
                                string condition = (parsedParameters.Trim());
                                if (logicOperatorParser.Parse(condition))
                                {
                                    i = loopList[loopIndex];
                                }
                                else
                                    loopIndex--;
                            }
                            continue;
                        }
                        else if (command == "if")
                        {
                            //check if the command is single line and multiline
                            Regex pattern = new Regex(@"(.+?) then (.+)");
                            Match match = pattern.Match(parameters.Trim());
                            if (match.Success)
                            {
                                if (logicOperatorParser.Parse(match.Groups[1].Value))
                                {
                                    commandParser.parse(match.Groups[2].Value.Trim(), g);
                                }
                                continue;
                            }
                            string condition = (parameters.Trim());
                            if (!logicOperatorParser.Parse(condition))
                            {
                                runProgram = false;
                            }
                            continue;
                        }
                        else if (command == "endif")
                        {
                            runProgram = true;
                            continue;
                        }
                        //check method syntax and if method is already given 
                        else if (command == "method")
                        {
                            if (parameters != null)
                            {
                                methodRunning = true;
                                if (SaveFunction.Savefunction.ContainsKey(command))
                                {
                                    MessageBox.Show("Function Already Exists", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    break;
                                }
                                else
                                {
                                    try
                                    {
                                        SaveFunction.Savefunction.Add(parameters.Split(',')[0].Trim(), i);
                                    }
                                    catch (Exception)
                                    {
                                        MessageBox.Show("Error defining Function", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                        break;
                                    }
                                }
                                continue;
                            }
                        }
                        else if (command == "endmethod")
                        {
                            i = tempProgramIndex;
                            tempProgramIndex = 0;
                            continue;
                        }
                        else if (command == "call")
                        {
                            tempProgramIndex = i;
                            i = SaveFunction.SearchFunction(parameters.Split(',')[0].Trim());
                            continue;
                        }
                        else
                        {
                            if (runProgram)
                            {
                                commandParser.parse(textBox_multiLine_cmd.Lines[i].Trim(), g);
                            }
                        }

                    }
                }
                catch (Exception er)
                {
                    MessageBox.Show(er.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
                }
            }

        }

        private void textBox_singleLine_cmd_KeyDown(object sender, KeyEventArgs e)
        {
            string command = textBox_singleLine_cmd.Text.ToLower().Split(',')[0];
            if (e.KeyCode == Keys.Enter)
            {
                switch (command)
                {
                    case "clear":
                        g.Clear(Color.LightGray);
                        commandParser.clear();
                        break;
                    case "reset":
                        g.Clear(Color.LightGray);
                        commandParser.clear();
                        textBox_multiLine_cmd.Text = "";
                        textBox_singleLine_cmd.Text = "";
                        break;
                    case "run":
                        button_Run_Click(sender, e);
                        break;
                    default:
                        MessageBox.Show("Unknown Command", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        break;
                }
                e.SuppressKeyPress = true;
            }

        }

        private void newFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            g.Clear(Color.LightGray);
            textBox_singleLine_cmd.Text = "";
            textBox_multiLine_cmd.Text = "";
            commandParser.clear();
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveFile();
        }

        private bool saveFile()
        {
            string code = textBox_multiLine_cmd.Text;
            if (filePath == null)
            {
                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.Filter = "gpl files (*.gpl)|*.gpl|All files (*.*)|*.*";
                saveFileDialog.Title = "Save GPL";
                saveFileDialog.ShowDialog();

                if (saveFileDialog.FileName != "")
                {
                    filePath = saveFileDialog.FileName;
                    using (StreamWriter sw = new StreamWriter(filePath))
                        sw.WriteLine(code);
                    fileSaved = true;
                    return true;
                }
                else
                {
                    return false;
                }
            }

            else
            {
                using (StreamWriter sw = new StreamWriter(filePath))
                    sw.WriteLine(code);
                fileSaved = true;
                return true;
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!fileSaved)
            {
                DialogResult dialogResult = MessageBox.Show("Save current file before proceeding?", "Changes not saved", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);
                if (dialogResult == DialogResult.Yes)
                {
                    if (!saveFile())
                        return;
                }
                else if (dialogResult == DialogResult.Cancel)
                {
                    return;
                }
            }
            Application.Exit();
        }

        private void openFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!fileSaved)
            {
                DialogResult dialogResult = MessageBox.Show("Save current file before proceeding?", "Changes not saved", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);
                if (dialogResult == DialogResult.Yes)
                {
                    if (!saveFile())
                        return;
                }
                else if (dialogResult == DialogResult.Cancel)
                {
                    return;
                }
            }
            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.InitialDirectory = "c:\\";
                openFileDialog.Filter = "gpl files (*.gpl)|*.gpl|All files (*.*)|*.*";
                openFileDialog.FilterIndex = 2;
                openFileDialog.RestoreDirectory = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    filePath = openFileDialog.FileName;

                    var fileStream = openFileDialog.OpenFile();

                    using (StreamReader reader = new StreamReader(fileStream))
                    {
                        textBox_multiLine_cmd.Text = reader.ReadToEnd();
                    }

                    fileSaved = true;
                }
            }
        }

        private void aboutUsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Graphical Programming Language - Application\n By Prasiddhi Bhattarai", "About", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void toolTip1_Popup(object sender, PopupEventArgs e)
        {

        }
    }
}
