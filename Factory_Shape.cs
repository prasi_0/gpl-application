﻿using System;

namespace GPL___Application
{
    /// <summary>
    /// Factory class to produce each shapes Circle, Rectangle and Triangle as needed
    /// </summary>
    class Factory_Shape
    {
        public Shape getShape(String shapeName)
        {
            shapeName = shapeName.ToUpper().Trim();

            if (shapeName.Equals("CIRCLE"))
            {
                return new Circle();
            }
            else if (shapeName.Equals("RECTANGLE"))
            {
                return new Rectangle();

            }
            else if (shapeName.Equals("TRIANGLE"))
            {
                return new Triangle();
            }
            else if (shapeName.Equals("SQUARE"))
            {
                return new Sqaure();
            }
            else if (shapeName.Equals("DRAWTO"))
            {
                return new DrawTo();
            }
            else
            {
                System.ArgumentException argEx = new System.ArgumentException("Fatal error:" +  shapeName +  "does not exist!");
                throw argEx;
            }

        }

    }
}
