﻿using System.Drawing;
using System.Windows.Forms;
using System.Threading;

namespace GPL___Application
{

    /// <summary>
    /// Create class Circle extending class Shape 
    /// for using its parameterized constructors
    /// </summary>
    public class Circle : Shape
    {
        int radius;

        /// <summary>
        /// Extend the class Shape and refer as base class 
        /// </summary>
        public Circle() : base()
        {

        }
        /// <summary>
        /// New parameter radius added to list of params in base class
        /// </summary>
        /// <param name="color"></param>
        /// <param name="fill"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="radius"></param>
        public Circle(Color color, bool fill, int x, int y, int radius) : base(color, fill, x, y)
        {
            this.radius = radius;
        }
        /// <summary>
        /// New parameter flash added to list of params in base class
        /// </summary>
        /// <param name="flash"></param>
        /// <param name="fill"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="radius"></param>
        public Circle(string flash, bool fill, int x, int y, int radius) : base(flash, fill, x, y)
        {
            this.radius = radius;
        }
        /// <summary>
        /// New implementation of set from base class
        /// </summary>
        /// <param name="color"></param>
        /// <param name="fill"></param>
        /// <param name="list"></param>
        public override void set(Color color, bool fill, params int[] list)
        {
            base.set(color, fill, list[0], list[1]);
            this.radius = list[2];
        }
        /// <summary>
        /// New implementation of set with flash from base class
        /// </summary>
        /// <param name="flash"></param>
        /// <param name="fill"></param>
        /// <param name="list"></param>
        public override void set(string flash, bool fill, params int[] list)
        {
            base.set(flash, fill, list[0], list[1]);
            this.radius = list[2];
        }
        /// <summary>
        /// Draw method which draws the shape with
        /// condition between fill or flash 
        /// </summary>
        /// <param name="g"></param>
        public override void draw(Graphics g)
        {

            if (fill)
            {
                if (flash != null)
                {
                    Thread newThread;
                    switch (flash)
                    {
                        case "redgreen":
                            newThread = new Thread(() => GetFlash(g, Color.Red, Color.Green));
                            newThread.IsBackground = true;
                            newThread.Start();
                            break;
                        case "blueyellow":
                            newThread = new Thread(() => GetFlash(g, Color.Blue, Color.Yellow));
                            newThread.IsBackground = true;
                            newThread.Start();
                            break;
                        case "blackwhite":
                            newThread = new Thread(() => GetFlash(g, Color.Black, Color.White));
                            newThread.IsBackground = true;
                            newThread.Start();
                            break;
                        default:
                            MessageBox.Show("Invalid color value", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            break;
                    }
                }
                else
                {
                    SolidBrush b = new SolidBrush(color);
                    g.FillEllipse(b, x, y, radius * 2, radius * 2);
                }
            }
            else
            {
                Pen p = new Pen(color, 2);
                g.DrawEllipse(p, x, y, radius * 2, radius * 2);
            }
        }

        /// <summary>
        /// Methos to get the shape flashing
        /// </summary>
        /// <param name="g"></param>
        /// <param name="first"></param>
        /// <param name="second"></param>
        private void GetFlash(Graphics g, Color first, Color second)
        {
            bool flag = false;
            while (true)
            {
                lock (g)
                {
                    if (flag == false)
                    {
                        SolidBrush b = new SolidBrush(first);
                        g.FillEllipse(b, x, y, radius * 2, radius * 2);
                        flag = true;
                    }
                    else
                    {
                        SolidBrush b = new SolidBrush(second);
                        g.FillEllipse(b, x, y, radius * 2, radius * 2);
                        flag = false;
                    }
                }
                Thread.Sleep(500);
            }
        }

        /// <summary>
        /// Overrides ToString method to convert input to string
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return base.ToString() + " " + this.radius;
        }
    }
}
