﻿using System.Drawing;
using System.Threading;
using System.Windows.Forms;

namespace GPL___Application
{
    /// <summary>
    /// Class Shape is extended to create Rectangle class 
    /// </summary>
    public class Rectangle : Shape
    {
        int width, height;

        /// <summary>
        /// Reuse and access base class by extending it
        /// </summary>
        public Rectangle() : base()
        {

        }
        /// <summary>
        /// Added all parameters needed to draw a rectangle 
        /// </summary>
        /// <param name="color"></param>
        /// <param name="fill"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        public Rectangle(Color color, bool fill, int x, int y, int width, int height) : base(color, fill, x, y)
        {
            this.width = width;
            this.height = height;
        }
        /// <summary>
        /// New parameter flash added to list of params in base class
        /// </summary>
        /// <param name="flash"></param>
        /// <param name="fill"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        public Rectangle(string flash, bool fill, int x, int y, int width, int height) : base(flash, fill, x, y)
        {
            this.width = width;
            this.height = height;
        }
        /// <summary>
        /// Virtual method is ovveridden to set the list of params 
        /// </summary>
        /// <param name="color"></param>
        /// <param name="fill"></param>
        /// <param name="list"></param>
        public override void set(Color color, bool fill, params int[] list)
        {
            base.set(color, fill, list[0], list[1]);
            this.width = list[2];
            this.height = list[3];
        }
        /// <summary>
        /// New implementation of set with flash from base class
        /// </summary>
        /// <param name="flash"></param>
        /// <param name="fill"></param>
        /// <param name="list"></param>
        public override void set(string flash, bool fill, params int[] list)
        {
            base.set(flash, fill, list[0], list[1]);
            this.width = list[2];
            this.height = list[3];
        }
        /// <summary>
        /// Draw method which draws the shape with
        /// condition between fill or flash 
        /// </summary>
        /// <param name="g"></param>
        public override void draw(Graphics g)
        {
            if (fill)
            {
                if (flash != null)
                {
                    Thread newThread;
                    switch (flash)
                    {
                        case "redgreen":
                            newThread = new Thread(() => GetFlash(g, Color.Red, Color.Green));
                            newThread.IsBackground = true;
                            newThread.Start();
                            break;
                        case "blueyellow":
                            newThread = new Thread(() => GetFlash(g, Color.Blue, Color.Yellow));
                            newThread.IsBackground = true;
                            newThread.Start();
                            break;
                        case "blackwhite":
                            newThread = new Thread(() => GetFlash(g, Color.Black, Color.White));
                            newThread.IsBackground = true;
                            newThread.Start();
                            break;
                        default:
                            MessageBox.Show("Invalid color value", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            break;
                    }
                }
                else
                {
                    SolidBrush b = new SolidBrush(color);
                    g.FillRectangle(b, x, y, width, height);
                }
            }
            else
            {
                Pen p = new Pen(Color.Black, 2);
                g.DrawRectangle(p, x, y, width, height);
            }
        }
        /// <summary>
        /// Methos to get the shape flashing
        /// </summary>
        /// <param name="g"></param>
        /// <param name="first"></param>
        /// <param name="second"></param>
        private void GetFlash(Graphics g, Color first, Color second)
        {
            bool flag = false;
            while (true)
            {
                lock (g)
                {
                    if (flag == false)
                    {
                        SolidBrush b = new SolidBrush(first);
                        g.FillRectangle(b, x, y, width, height);
                        flag = true;
                    }
                    else
                    {
                        SolidBrush b = new SolidBrush(second);
                        g.FillRectangle(b, x, y, width, height);
                        flag = false;
                    }
                }
                Thread.Sleep(500);
            }
        }
        /// <summary>
        /// Overrides ToString method to convert input to string
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return base.ToString() + " " + this.width + " " + this.height;
        }

    }
}
